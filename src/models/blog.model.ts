export class Blog{
    created_at: string;
    story_id: number;
    story_title: string;
    story_url: string;
    title: string;
    url: string;
}