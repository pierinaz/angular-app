import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Blog } from './../models/blog.model'

@Injectable({
    providedIn: 'root'
  })
export class BlogService {

    constructor(private httpClient: HttpClient) {}
  
    getBlogs() {
        let apiURL = 'http://localhost:3000/blogs';
        return this.httpClient.get<Blog[]>(apiURL);
    }

    public deleteBlog(id: number){
        let apiURL = 'http://localhost:3000/blog';
        let header = new HttpHeaders();
        header.append('content-type','application/json');
        return this.httpClient.delete(`${apiURL}?id=${id}`, { headers: header });
    }

    getTimeFormatted(hours, minutes){
      if(hours === 0){
          return 12 + ':' + minutes + ' pm'; 
      } else if(hours < 24 && hours > 12 ){
          return (hours - 12) + ':' + minutes + ' pm'; 
      } else {
          return hours + ':' + minutes + ' am'; 
      }
   }

   setDate(blogs){
    let options = {  month: 'long', day: 'numeric' };
    let dateTimeFormat = new Intl.DateTimeFormat('en-US', options);
    const today = new Date();
    const yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    let minutes = 0;
    blogs.map((blog) => {
        const dateBlog = new Date(blog.created_at);

        if(dateBlog.toDateString() === today.toDateString()){
            minutes = dateBlog.getMinutes();
            blog.date = this.getTimeFormatted(dateBlog.getHours(), (minutes<10?'0':'') + minutes.toString());
        } else if(dateTimeFormat.format(dateBlog) === dateTimeFormat.format(yesterday)) {
          blog.date = 'Yesterday';
        } else {       
            blog.date = dateTimeFormat.format(dateBlog);
        }
    })

      return blogs;
    }
  }