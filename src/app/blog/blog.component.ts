import { Component } from "@angular/core";
import { BlogService } from "./../../services/blog.service";


@Component({
    selector: 'app-blog',
    templateUrl: './blog.component.html',
    styleUrls: ['./blog.component.css'],
    providers:  [ BlogService ]
})

export class BlogComponent { 
    blogs = [];
    errorMessage: string;
    successMessage: string;

    constructor(private apiService: BlogService){}

    ngOnInit(){
        this.getBlogs();
     }

     deleteBlog(id: number){
        if (window.confirm('Are you sure, you want to delete?')){
            this.apiService.deleteBlog(id).subscribe((res)=>{
                console.log("Deleted a blog");
                this.successMessage = 'The blog was delete succefully';
                this.getBlogs();
            },
            () => this.errorMessage = "Error deleting the blog.");
        }
     }

     getBlogs(){
        this.apiService.getBlogs().subscribe((res)=>{
            this.blogs = this.apiService.setDate(res);
      },
      () => this.errorMessage = "Error finding blogs.");
     }
}