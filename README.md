**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Some instructions
## Development server
1. Install dependencies: npm install / npm i
2. Command for start app: npm start / ng serve
3. Open at: http://localhost:4200
3. I had some issues for delete method, i used a chrome extension for cors error: Allow-Control-Allow-Origin: *
---

#Comments

1. I used a predifined command for new Angular App: ng new my-app 
2. The configuration of this app: Angular 7 / WebPack
